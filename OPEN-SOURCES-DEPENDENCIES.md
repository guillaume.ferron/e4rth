# Les logiciels Open Sources modifiées dans ce projet

## ev3dev-lang-python-demo  > EXPLOR3R
## Homepage : https://github.com/ev3dev/ev3dev-lang-python-demo
### Status : A ETE MODIFIE
- Le dossier a été de-gitté
- le fichier a été renommé de auto-drive.py vers auto_drive.y, afin de permettre son import dans d'autres fichiers
- la manière d'intégrer le projet à été la suivante : 
	- le code original a été conservé et laissé inchangé
	- les fonctions du fichier ont été déplacés dans une class DenisDemidov (nom de l'auteur du code)
	- le thread d'execution principal a été conditionné à __name__ == __main__ afin de garantir que le fichier puisse être utilisé sans effets de bord
	- nous créons une classe move qui étend la classe DenisDemidov

## ev3dev-mocks/
### Status : N'A PAS ETE MODIFIE / POURRAIT ETRE MODIFIE A L'AVENIR
- Le dossier a été de-gitté
pas d'utilisation en cours mais une prevision d'utilisation
une nouvelle installation est en cours pour intégrer cette dépendance en vue de produire un environnement de développement découplé de la machine d'excution. Elle demande spécifiquement une Ubuntu pour pouvoir etre utilisé. L'installation de cette Ubuntu est en cours à cette heure (status : mimo 4).


