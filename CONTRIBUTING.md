### Liste des contributeurs
Axelle Vinckx avinckx@yahoo.fr
Imane Wadgattait imane.wadgattait@live.fr
Pierre Courtois courtois.pierre@ymail.com
Guillaume Ferron guillaume.ferron@gmail.com
La Sorbonne - Master Mimo UFR09

### Contribuer à E4rth
Merci de vouloir contribuer. Nous vous invitons à faire remonter [ici](https://gitlab.com/guillaume.ferron/e4rth/issues) les problèmes rencontrés lors du l'utilisation du logiciel.

