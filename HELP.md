
# Ce que fait le projet
## -> Voir le fichier e4rth/README.md

# Les noms des personnes y ayant contribué
## -> Voir le fichier e4rth/CONTRIBUTING.md

# Une procédure d'installation (version de python, librairies à installer et tout ce qui est nécessaire pour qu'une autre personne - donc moi - puisse le faire fonctionner)
## -> Voir le fichier e4rth/HOW-TO-START.md

# Si vous êtes repartis d'un projet externe que vous avez complété:
#### Il faut indiquer quel est ce projet
#### Ce que vous y avez ajouté
#### Bref, je dois être capable de différencier votre code de celui qui vous a servi de base de travail
## -> Voir le fichier e4rth/OPEN-SOURCE-DEPENDENCIES.md

# Une documentation expliquant comment il se démarre et quelques captures d'écran montrant à quoi il ressemble si jamais je n'arrivais pas à le faire fonctionner
## voir mimorobot
## sachant qu'il n'est pas possible de le faire fonctionner sans le matériel OU sans une ubuntu installé par mimorobot capable d'implémenter les Mocks d'EV3dev (voir OPEN-SOURCE-DEPENDENCIES pour plus d'info)
## Pour les captures d'écran il y a des photos liées au projet ici :
- [lien 1](https://drive.google.com/open?id=1NLeRPvwYQCdRsF6AcMRUwTBAUA8hXdi_)
- [lien 2](https://drive.google.com/drive/folders/1I8FHDdsyxiDnQs2HFVYZFxDcx-hjxDK9?usp=sharing)


