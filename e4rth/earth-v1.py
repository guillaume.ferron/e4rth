
import time
from smbus import SMBus
from ev3dev2.motor import MediumMotor, OUTPUT_B
import cv2

TARGET = "banana"

TARGETS = {
    "banana" : "yellow"
}

COLORS = {
    "yellow" : 2
}

bus = SMBus(6) 
bus.write_byte_data(0x01, 0x41, 0x42)
# total = bus.read_byte_data(0x01, 0x42)  # get number of tracked objects

camera = MediumMotor(OUTPUT_B)

position = 0

def refine_measure(m):
    n = bus.read_i2c_block_data(0x01, 0x44)[0]
    time.sleep(0.1)
    e = bus.read_i2c_block_data(0x01, 0x45)[0]
    time.sleep(0.1)
    s = bus.read_i2c_block_data(0x01, 0x46)[0]
    time.sleep(0.1)
    w = bus.read_i2c_block_data(0x01, 0x47)[0]
    time.sleep(0.1)

    return (n-m[0], e-m[1], s-m[2], w-m[3])


def mesure():
    n = bus.read_i2c_block_data(0x01, 0x44)[0]
    time.sleep(0.1)
    e = bus.read_i2c_block_data(0x01, 0x45)[0]
    time.sleep(0.1)
    s = bus.read_i2c_block_data(0x01, 0x46)[0]
    time.sleep(0.1)
    w = bus.read_i2c_block_data(0x01, 0x47)[0]
    time.sleep(0.1)

    return refine_measure((n, e, s, w))

def get_color():
    n = bus4.read_byte_data(0x42)
    data = bus4.read_i2c_block_data(0x43, 5)
    color = data[0]
    return color

def get_picture():

    camera = cv2.VideoCapture(0)
    camera.set(cv2.CAP_PROP_FRAME_WIDTH, 240); 
    camera.set(cv2.CAP_PROP_FRAME_HEIGHT, 160);
    camera.set(cv2.CAP_PROP_SATURATION,0.2);

    noerror, image = camera.read()
    if noerror == False:
        print("Warning : photo failed")
        return

    cv2.imwrite('view.jpeg', image) 
    print("NOTE : The above error comes from a minor known bug in libjpeg that has never been patched")

    del(camera)

def detect_target():
    if get_color() == COLORS[TARGETS[TARGET]]:
        return true
    else:
        return false

def front(distance = 0, power = 0):
    pass
    
def right(distance = 225, power = 200):
    camera.run_timed(speed_sp=power, time_sp=distance)
    time.sleep(0.1)

def left(distance = 225, power = 200):
    camera.run_timed(speed_sp=-power, time_sp=distance)
    time.sleep(0.1)

def look (direction):
    global position 

    while not position == direction:
        if position < direction:
            right()
            position = position + 1
        else:
            left()
            position = position - 1


def walk(cartography):
    for direction in cartography:
        look(direction)

def lock(target):
    r = measure_object()
    print("lock")

    # get_picture()

    while ( r < target and r > target) and detect_target():
        r = measure_object()
        print("Found %s" % TARGET)

    print("unlock")

def process(cbk):
    cbk()

def measure_object():
    m = mesure()
    return m[0] + m[2] + m[1] + m[3]

def search_left():
    look(-1)
    rez = measure_object()
    return rez

def search_right():
    look(1)
    rez = measure_object()
    return rez

def search_front():
    look(0)
    rez = measure_object()
    return rez

def select_target(r1, r2, r3):
    best = sorted([r1, r2, r3])
    best2 = []
    for item in best:
        if item > 0:
            best2.insert(0,item)
        else:
            best2.append(item)
    return best2

def search_target():

    print("searching")

    # we search front
    r1 = search_front()

    # we search left
    r2 = search_left()

    # we search right
    r3 = search_right()

    best = select_target(r1, r2, r3)
    print(best)

    # Test
    r1 = 1

    best = select_target(r1, r2, r3)
    print(best)

    # we decie
    if len(best) > 0:
        if best[0] == r1 and r1 > 0:
            print("found best : front")
            look(0)
            lock(position)
        elif best[1] == r2 and r2 > 0:
            print("found best : left")
            look(-1)
            lock(position)
        elif best[2] == r3 and r3 > 0:
            print("found best : right")
            look(1)
            lock(position)

def test():
    def run(val):
        print(val)
        best = select_target(val[0], val[1], val[2])
        print(best)
        if len(best) > 0:
            if best[0] == val[0] and val[0] > 0:
                print("found best : front")
                look(0)
                lock(position)
            elif best[1] == val[1] and val[1] > 0:
                print("found best : left")
                look(-1)
                lock(position)
            elif best[2] == val[2] and val[2] > 0:
                print("found best : right")
                look(1)
                lock(position)
        print("---------")
        time.sleep(1)

    run([1, 0, 0])
    run([0, 1, 0])
    run([0, 0, 1])

while True:
    # print("test start")
    # test()
    # print("test end")
    search_target()
    # pass
