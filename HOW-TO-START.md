# How to start?

The process to install everything is very complicated
We push a "requirements.txt" in e4rth/e4rth, you can :
```
cd e4rth/e4rth$
pip3 install -r requirements.txt
```

But we advise you execute `./mimorobot 1` instead.
This bash script will give you all the dependencies you need

## What you will need if you use mimorobot
- you computer
- a 64bits dedicated computer
- a Lego bloc
- a NxtCamv5 camera
- a 8go sd Card
- a 32go sd Card
