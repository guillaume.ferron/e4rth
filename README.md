
E4rth
Projet réalisé dans le cadre du master 2 Métiers de l'informatique et maitrise d'ouvrage de l'UFR09 à La Sorbonne.
E4rth est une implémentation de la distribution ev3dev au sein d'une brique intelligente Lego doté des librairies suivantes :
- Capteur NxtCamv5
- Webcam premier-prix
- Large Moteurs Lego x2
- Medium Motor lego (rotatif)
- Module smbus
- Package EXPLOR3R
- Module ev3dev2
- Module python-opencv
- Module imageai
- Module tensorflow cross-compilé dans un docker sur une machine externe

Le produit final se présente sous la forme d'un robot capable de se déplacer au hasard dans son environnement, tournant la "tête" dès qu'il repère un objet "intéressant". Lorsque cet événement se produit le robot s'approche de l'objet et tente de le définir. Dans la v1 e4rth ne pouvait pas encore se déplacer et disposait d'un dictionnaire contenant des mots courants ("pomme", "banane", ...) associés aux codes couleurs utilisés par la caméra. Dans la v2, tensorflow a été ajouté de manière à ce que e4rth reconnaisse des objets de manière très précise, à partir d'une photo qu'il prend avec la webcam.

E4rth est livré avec Mimorobot, un programme bash tournant dans une console Unix visant à automatiser l'installation de l'environnement, laquelle a été la partie la plus difficile du projet.


Si vous êtes repartis d'un projet externe que vous avez complété:

    Il faut indiquer quel est ce projet
    Ou sont les sources de ce projet
    Ce que vous y avez ajouté
    Bref, je dois être capable de différencier votre code de celui qui vous a servi de base de travail

Une documentation expliquant comment il se démarre et quelques captures d'écran montrant à quoi il ressemble si jamais je n'arrivais pas à le faire fonctionner
